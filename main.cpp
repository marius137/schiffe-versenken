#include <iostream>
#include "common.h"
#include "main.h"


int main() {
    //Initialisierung der Boat und Screen Klassen
    auto *player1Screen = new playfield(unknown);
    auto *player1Boats = new playfield(water);
    auto *player2Screen = new playfield(unknown);
    auto *player2Boats = new playfield(water);

    int playerCount = 1;

    std::cout << "Willkomen bei Schiffe Versenken" << std::endl;
    std::cout << "Spieler 1 Bitte Boote Platzieren" << std::endl;
    placeBoats(player1Boats);
    clearScreen();
    std::cout << "Spieler 2 Bitte Boote Platzieren" << std::endl;
    placeBoats(player2Boats);
    clearScreen();

    do {
        if (playerCount % 2 == 1) {
            readyScreen(1);
            printField(player1Boats, player1Screen);
            playerAttack(player2Boats, player1Screen, 1);
            printField(player1Boats, player1Screen);
            playerCount++;
            waitForInput();
            clearScreen();
        } else {
            readyScreen(2);
            printField(player2Boats, player2Screen);
            playerAttack(player1Boats, player2Screen, 2);
            printField(player2Boats, player2Screen);
            playerCount++;
            waitForInput();
            clearScreen();
        }

    } while (player1Boats->stillAlive() && player2Boats->stillAlive());
    if(player1Boats->stillAlive()){ //Anderer Check wer gewonnen hat
        std::cout<<"Spieler 1 hat Gewonnen!"<<std::endl;
    }else{
        std::cout<<"Spieler 2 hat Gewonnen!"<<std::endl;
    }
    return 0;
}

void playerAttack(playfield *boats, playfield *screen, int player) {
    int tempX;
    int tempY;
    char tempChar;
    std::string tempAttack;

    std::cout << "Spieler " << player << std::endl;
    std::cout << "Geben sie bitte das Feld an das sie Angreifen wollen" << std::endl;

    do {
        std::getline(std::cin, tempAttack);
        if (parseCoords(tempAttack, &tempX, &tempY)) {
            break;
        } else {
            std::cout << "Bitte gib ein Feld an!" << std::endl;
        }

    } while (true);

    tempChar = boats->hitSquare(tempX, tempY);
    screen->setSquare(tempX, tempY, tempChar);
    screen->printField();
    //                                                      VVVVVV Check ob gewonnen wurde
    if ((tempChar == ship_hit || tempChar == ship_dead)&&boats->stillAlive()) {
        playerAttack(boats, screen, player);
    }

}

void printField(playfield *playfield1, playfield *playfield2) {
    std::cout << "Deine Boote           Gegnerische Boote" << std::endl;
    std::cout << " 12345678910" << "          " << " 12345678910" << std::endl;
    for (int y = 0; y < fieldSize; y++) {
        std::cout << (char) (y + 97) << playfield1->getData(y);
        std::cout << "           ";
        std::cout << (char) (y + 97) << playfield2->getData(y) << std::endl;
    }
}

void placeBoats(playfield *boats) {
    //TODO verifikation
    std::string input, temp;
    int selection;
    int i = 0;
    int xcoord, ycoord, direction;
    int boatNumbers[5] = {0, 4, 3, 2, 1};
    while (i < 10) {
        //clearScreen();
        boats->printField();
        do {
            std::cout << "1er:" << boatNumbers[1] << " 2er:" << boatNumbers[2] << " 3er:" << boatNumbers[3] << " 4er:"
                      << boatNumbers[4] << std::endl;
            std::cout << "länge?" << std::endl;
            std::getline(std::cin, input);
            selection = stoi(input);
        } while (!(selection > 0 && selection < 5 && boatNumbers[selection] > 0));
        std::cout << "Koordinaten? ";
        std::getline(std::cin, temp);
        parseCoords(temp, &xcoord, &ycoord);
        std::cout << std::endl;
        std::cout << "Richtung ?" << std::endl << "1: Links" << std::endl << "2: Rechts" << std::endl << "3: Hoch"
                  << std::endl << "4: Runter" << std::endl;
        std::getline(std::cin, temp);
        direction = std::stoi(temp);
        direction--;
        std::cout << std::endl;
        if (boats->placeBoat(xcoord, ycoord, selection, direction)) {
            i++;
            boatNumbers[selection]--;
        }
    }
}

bool parseCoords(std::string input, int *x, int *y) {
    int xcoord, ycoord;
    if (input.length() > 3 || input.length() < 2) {
        return false;
    }
    std::string xcoords = input.substr(0, 1);
    ycoord = std::stoi(input.substr(1, input.length()));
    xcoord = lookup(xcoords);
    *x = xcoord - 1;
    *y = ycoord - 1;
    return true;
}

int lookup(std::string s) {
    std::string lut[] = {"", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j"};
    for (int i = 0; i < fieldSize + 1; i++) {
        if (s == lut[i]) {
            return i;
        }
    }
    return -1;
}

void clearScreen() {
    system("clear");
    //std::cout << std::string(100, '\n');
}

void readyScreen(int n) {
    std::string whatever;
    std::cout << "Spieler " << n << " Bereit?" << std::endl;
    std::getline(std::cin, whatever);
}
void waitForInput(){
    std::string whatever;
    std::getline(std::cin, whatever);
}