//
// Created by marius on 05.12.20.
//

#ifndef SCHIFFE_VERSENKEN_MAIN_H
#define SCHIFFE_VERSENKEN_MAIN_H

#include "playfield.h"

void playerAttack(playfield *boats, playfield *screen, int player);

void printField(playfield *playfield1, playfield *playfield2);

void placeBoats(playfield *boats);

bool parseCoords(std::string input, int *x, int *y);

int lookup(std::string s);

void clearScreen();

void readyScreen(int n);

void waitForInput();

#endif //SCHIFFE_VERSENKEN_MAIN_H
