//
// Created by marius on 02.12.20.
//
#pragma once
#ifndef SCHIFFE_VERSENKEN_COMMON_H
#define SCHIFFE_VERSENKEN_COMMON_H

#define fieldSize 10
#define ship_max_length 4

#define unknown '+'
#define water '~'
#define ship 'B'
#define ship_hit 'b'
#define ship_dead 'X'

#define dir_left 0
#define dir_right 1
#define dir_up 2
#define dir_down 3

#endif //SCHIFFE_VERSENKEN_COMMON_H
