//
// Created by marius on 02.12.20.
//

#ifndef SCHIFFE_VERSENKEN_PLAYFIELD_H
#define SCHIFFE_VERSENKEN_PLAYFIELD_H


class playfield {
public:
    explicit playfield(char type);

    char hitSquare(int x, int y);

    void setSquare(int x, int y, char object);

    bool placeBoat(int x, int y, int length, int direction);

    void printField();

    std::string getData(int row);

    bool stillAlive();

private:
    char data[fieldSize][fieldSize]; //[row][column]

    bool isShipDead(int x, int y);

    void replaceShip(int x, int y);

    bool checkBox(int x, int y, int l, int h);
};


#endif //SCHIFFE_VERSENKEN_PLAYFIELD_H
