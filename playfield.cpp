//
// Created by marius on 02.12.20.
//
#include <iostream>
#include "common.h"
#include "playfield.h"


char playfield::hitSquare(int x, int y) {
    if (data[x][y] == ship) {
        data[x][y] = ship_hit;
        if (isShipDead(x, y)) {
            replaceShip(x, y);
        }
    }
    return data[x][y];
}

void playfield::setSquare(int x, int y, char object) {
    data[x][y] = object;
    if (object == ship_dead) {      //Korrekt prüfen ob das boot im screen tot ist
        if (isShipDead(x, y)) {
            replaceShip(x, y);
        }
    }
}

playfield::playfield(char type) {
    for (int i = 0; i < fieldSize; i++) {
        for (int j = 0; j < fieldSize; j++) {
            data[i][j] = type;
        }
    }
}


bool playfield::placeBoat(int row, int column, int length, int direction) {
    //werte die zu groß und zu klein sind rausfiltern
    if (row > fieldSize || row < 0 || column > fieldSize || column < 0 || length < 1 || length > 4) {
        return false;
    }
    //werte rausfiltern bei denen das boot aus dem spielfeld hängen würde
    switch (direction) {
        case dir_left:
            if (column - length < 0) {
                return false;
            }
            if (!checkBox(column - length - 1, row - 1, length + 2, 3)) {
                return false;
            }
            for (int i = column; i > column - length; i--) {
                data[row][i] = ship;
            }
            break;
        case dir_right:
            if (column + length > fieldSize) {
                return false;
            }
            if (!checkBox(row - 1, column - 1, length + 2, 3)) {
                return false;
            }
            for (int i = column; i < column + length; i++) {
                data[row][i] = ship;
            }
            break;
        case dir_up:
            if (row - length < 0) {
                return false;
            }
            if (!checkBox(row - length - 1, column - 1, 3, length + 2)) {
                return false;
            }
            for (int i = row; i > row - length; i--) {
                data[i][column] = ship;
            }
            break;
        case dir_down:
            if (row + length > fieldSize) {
                return false;
            }
            if (!checkBox(row - 1, column - 1, 3, length + 2)) {
                return false;
            }
            for (int i = row; i < row + length; i++) {
                data[i][column] = ship;
            }
            break;
        default:
            return false;
    }
    return true;
}

bool playfield::checkBox(int x, int y, int l, int h) {
    if (x < 0) {
        x = 0;
    }
    if (y < 0) {
        y = 0;
    }
    if (x > fieldSize) {
        x = fieldSize;
    }
    if (y > fieldSize) {
        y = fieldSize;
    }
    for (int i = x; i < x + l; i++) {
        for (int j = y; j < y + h; j++) {
            if (i >= fieldSize || j >= fieldSize) {
                continue;
            }
            if (data[i][j] != water) {
                return false;
            }
        }
    }
    return true;
}

void playfield::printField() {
    std::cout << " 12345678910" << std::endl;
    for (int i = 0; i < fieldSize; i++) {
        std::cout << (char) (i + 97);
        for (int j = 0; j < fieldSize; j++) {
            std::cout << data[i][j];
        }
        std::cout << std::endl;
    }
}

std::string playfield::getData(int row) {
    std::string temp;
    for (int i = 0; i < fieldSize; i++) {
        temp += data[row][i];
    }
    return temp;
}


bool playfield::isShipDead(int x, int y) {
    //down
    for (int i = x; i < x + ship_max_length; i++) {
        if (i > fieldSize || i < 0) {
            continue;
        }
        if (data[i][y] == ship) {
            return false;
        } else if (data[i][y] == water) {
            break;
        }
    }
    //right
    for (int i = y; i < y + ship_max_length; i++) {
        if (i > fieldSize || i < 0) {
            continue;
        }
        if (data[x][i] == ship) {
            return false;
        } else if (data[x][i] == water) {
            break;
        }
    }
    //up
    for (int i = x; i > x - ship_max_length; i--) {
        if (i > fieldSize || i < 0) {
            continue;
        }
        if (data[i][y] == ship) {
            return false;
        } else if (data[i][y] == water) {
            break;
        }
    }
    //left
    for (int i = x; i > x - ship_max_length; i--) {
        if (i > fieldSize || i < 0) {
            continue;
        }
        if (data[x][i] == ship) {
            return false;
        } else if (data[x][i] == water) {
            break;
        }
    }
    return true;
}

void playfield::replaceShip(int x, int y) {
    //down
    for (int i = x; i < x + ship_max_length; i++) {
        if (i > fieldSize || i < 0) {
            continue;
        }
        if (data[i][y] == ship_hit) {
            data[i][y] = ship_dead;
        } else if (data[i][y] == water) {
            break;
        }
    }
    //right
    for (int i = y; i < y + ship_max_length; i++) {
        if (i > fieldSize || i < 0) {
            continue;
        }
        if (data[x][i] == ship_hit) {
            data[x][i] = ship_dead;
        } else if (data[x][i] == water) {
            break;
        }
    }
    //up
    for (int i = x; i > x - ship_max_length; i--) {
        if (i > fieldSize || i < 0) {
            continue;
        }
        if (data[i][y] == ship_hit) {
            data[i][y] = ship_dead;
        } else if (data[i][y] == water) {
            break;
        }
    }
    //left
    for (int i = y; i > y - ship_max_length; i--) {
        if (i > fieldSize || i < 0) {
            continue;
        }
        if (data[x][i] == ship_hit) {
            data[x][i] = ship_dead;
        } else if (data[x][i] == water) {
            break;
        }
    }
}

bool playfield::stillAlive() {
    for (int i = 0; i < fieldSize; i++) {
        for (int j = 0; j < fieldSize; j++) {
            if (data[i][j] == ship) {
                return true;
            }
        }
    }
    return false;

}